<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/17/2022
  Time: 11:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div align="center">
  <table border="1" cellpadding="5">
    <caption><h2>List of Users</h2></caption>
    <tr>
      <th>Name</th>
      <th>Action</th>
    </tr>
    <c:forEach var="user" items="${ListGroupUser}">
      <tr>
        <td><c:out value="${user.first_name}" /></td>
        <td>
          <a href="/addGroupUser?id=<c:out value='${user.id}' />">Add</a>

        </td>
      </tr>
    </c:forEach>
  </table>
  <h2>
    <a href="/exitFromAddUsers">Exit</a>
  </h2>
</div>
</body>
</html>
