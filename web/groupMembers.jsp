<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/18/2022
  Time: 3:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>Members</h2></caption>
        <tr>
            <th>Name</th>
            <th>Action</th>
        </tr>
        <c:forEach var="user" items="${listUser}">
            <tr>

                <td><c:out value="${user.first_name}" /></td>
                <td>
                    <a style="color:red" href="/deleteMember?userId=<c:out value='${user.id}'/>">Delete</a>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
        </c:forEach>
    </table>
    <h2>
        <a href="/exitFromGroupMembers">Exit</a>
    </h2>
</div>

</body>
</html>
