<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/17/2022
  Time: 11:13 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chat</title>
</head>
<body>
<center>
    <h1>Telegram</h1>

</center>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>Friends Messages</h2></caption>
        <tr>
            <th>Messages</th>
        </tr>
        <c:forEach var="message" items="${listMyFriendMessage}">
            <tr>

                <td><c:out value="${message.text}" /></td>

            </tr>
        </c:forEach>
    </table>

    <table border="1" cellpadding="5">
        <caption><h2>My Messages</h2></caption>
        <tr>
            <th>Messages</th>
        </tr>
        <c:forEach var="message" items="${listMessage}">
            <tr>
                <td><c:out value="${message.text}" /></td>
            </tr>
        </c:forEach>

    </table>
    <br>
    <form action="/fromId">
        <input type="text" placeholder="Enter text" name="text">
        <button>Send Message</button>
    </form>
</div>
<center>

    <h2>
        <a href="/logoutFromMessage">Logout</a>
    </h2>
</center>
</body>
</html>
