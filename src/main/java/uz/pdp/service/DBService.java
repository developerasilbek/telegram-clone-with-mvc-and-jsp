package uz.pdp.service;

import uz.pdp.model.Result;
import uz.pdp.model.User;

import java.sql.*;

public class DBService {
    String url = "jdbc:postgresql://localhost:5432/telegram";
    String dbUser = "postgres";
    String dbPassword = "root123";


    public Result registerUser(User user) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            String checkPhoneNumberQuery = "select count(*) from users where phone_number='" + user.getPhone_number() + "'";
            ResultSet resultSet = statement.executeQuery(checkPhoneNumberQuery);
            int countByFields = 0;
            while (resultSet.next()) {
                countByFields = resultSet.getInt(1);
            }
            if (countByFields > 0) {
                return new Result("Phone number already exist", false);
            }
            String query = "insert into users(first_name,last_name,phone_number,password,role_id,active)" +
                    " values('" + user.getFirst_name() + "','" + user.getLast_name() + "','" + user.getPhone_number() + "','" + user.getPassword() + "',2,true);";
            boolean execute = statement.execute(query);
            System.out.println(execute);
            return new Result("Successfully registered", false);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public User login(String phone_number, String password) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
            String query = "select u.*,r.name from users u join roles r on u.role_id=r.id where phone_number=? and password=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, phone_number);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String firstName = resultSet.getString(2);
                String lastName = resultSet.getString(3);
                phone_number = resultSet.getString(4);
                String roleName = resultSet.getString(8);
                User user = new User(
                        id,
                        firstName,
                        lastName,
                        phone_number,
                        null,
                        roleName,
                        true
                );
                return user;
            }
            return null;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
