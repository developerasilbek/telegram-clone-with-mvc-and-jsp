package uz.pdp.service;

import uz.pdp.model.Groups;
import uz.pdp.model.Messages;
import uz.pdp.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataBase {
    private String jdbcURL;
    private String jdbcUsername;
    private String jdbcPassword;
    private Connection jdbcConnection;

    public DataBase(String jdbcURL, String jdbcUsername, String jdbcPassword) {
        this.jdbcURL = jdbcURL;
        this.jdbcUsername = jdbcUsername;
        this.jdbcPassword = jdbcPassword;
    }


    public DataBase() {
    }

    private void connect() throws SQLException {
        if (jdbcConnection == null || jdbcConnection.isClosed()) {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }
            jdbcConnection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        }
    }

    protected void disconnect() throws SQLException {
        if (jdbcConnection != null && !jdbcConnection.isClosed()) {
            jdbcConnection.close();
        }
    }

    public List<User> listAllUsers(int user_id) throws SQLException {
        List<User> userList = new ArrayList<>();
        String sql = "Select * from users where id != ? and role_id!=1";

        connect();
        if (jdbcConnection != null) {
            PreparedStatement preparedStatement = jdbcConnection.prepareStatement(sql);
            preparedStatement.setInt(1, user_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String firstName = resultSet.getString(2);
                boolean active = resultSet.getBoolean(7);
                User user = new User(id, firstName, active);
                userList.add(user);
            }


            resultSet.close();
            preparedStatement.close();
        }

        disconnect();
        return userList;
    }

    public List<Messages> listOfMessagesByUserId(int fromId, int toId) throws SQLException {
        List<Messages> messagesList = new ArrayList<>();
        String query = "select text from messages where from_id=? and to_id=?;";
        connect();
        PreparedStatement preparedStatement = jdbcConnection.prepareStatement(query);
        preparedStatement.setInt(1, fromId);
        preparedStatement.setInt(2, toId);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            String text = resultSet.getString(1);
            Messages messages = new Messages(fromId, toId, text);
            messagesList.add(messages);
        }

        resultSet.close();
        preparedStatement.close();
        disconnect();

        return messagesList;
    }

    public boolean insertMessage(int fromId, int toId, String text) throws SQLException {
        String sql = "insert into messages(from_id,to_id,text) VALUES(?,?,?);";
        connect();

        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, fromId);
        statement.setInt(2, toId);
        statement.setString(3, text);


        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowInserted;
    }

    public boolean activeUserById(int user_id) throws SQLException {
        String sql = "Select active from users where id=?";
        connect();
        PreparedStatement preparedStatement = jdbcConnection.prepareStatement(sql);
        preparedStatement.setInt(1, user_id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            boolean active = resultSet.getBoolean(1);
            return active;
        }
        return false;
    }

    public boolean updateUserBlockAndUnblock(User user) throws SQLException {
        String sql = "update users set active=? where id=?";
        connect();

        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setBoolean(1, user.isActive());
        statement.setInt(2, user.getId());


        boolean rowUpdated = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowUpdated;
    }

    public boolean insertGroup(String name, int user_id) throws SQLException {
        String sql = "insert into groups(name,user_id) values(?,?);";
        connect();

        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setString(1, name);
        statement.setInt(2, user_id);


        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowInserted;
    }

    public List<Groups> listMyGroups(int user_id) throws SQLException {
        List<Groups> groupsList = new ArrayList<>();
        String query = "select id,name, user_id from groups where user_id=?;";
        connect();
        PreparedStatement preparedStatement = jdbcConnection.prepareStatement(query);
        preparedStatement.setInt(1, user_id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String name = resultSet.getString(2);
            user_id = resultSet.getInt(3);
            Groups groups = new Groups(id, name, user_id);
            groupsList.add(groups);
        }

        resultSet.close();
        preparedStatement.close();
        disconnect();

        return groupsList;
    }

    public List<User> listInsertUserForGroup(int user_id, int group_id) throws SQLException {
        List<User> userList = new ArrayList<>();
        String query = "select u.id, u.first_name from users u where role_id != 1 and id not in (select member_id from group_members where group_id = ?) and id != ?;";
        connect();
        PreparedStatement preparedStatement = jdbcConnection.prepareStatement(query);
        preparedStatement.setInt(1, group_id);
        preparedStatement.setInt(2, user_id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String first_name = resultSet.getString(2);
            User user = new User(id, first_name);
            userList.add(user);
        }

        resultSet.close();
        preparedStatement.close();
        disconnect();

        return userList;
    }

    public boolean insertMemberToGroup(int group_id, int member_id) throws SQLException {
        String sql = "insert into group_members(group_id,member_id) values(?,?);";
        connect();

        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, group_id);
        statement.setInt(2, member_id);


        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowInserted;
    }


    public List<User> listOfGroupMembers(int user_id, int group_id) throws SQLException {
        List<User> userList = new ArrayList<>();
        String query = "select u.id, u.first_name from users u where role_id != 1 and id  in (select member_id from group_members where group_id = ?) and id != ?;";
        connect();
        PreparedStatement preparedStatement = jdbcConnection.prepareStatement(query);
        preparedStatement.setInt(1, group_id);
        preparedStatement.setInt(2, user_id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String first_name = resultSet.getString(2);
            User user = new User(id, first_name);
            userList.add(user);
        }

        resultSet.close();
        preparedStatement.close();
        disconnect();

        return userList;
    }

    public boolean deleteMembersFromGroup(int group_id, int member_id) throws SQLException {
        String sql = "delete from group_members where group_id=? and member_id=?";

        connect();

        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, group_id);
        statement.setInt(2, member_id);

        boolean rowDeleted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowDeleted;
    }

    public List<Groups> listAddedGroup(int user_id) throws SQLException {
        List<Groups> groupsList = new ArrayList<>();
        String query = "select g.id, g.name from groups g join group_members gm on gm.group_id = g.id where gm.member_id=?;";
        connect();
        PreparedStatement preparedStatement = jdbcConnection.prepareStatement(query);
        preparedStatement.setInt(1, user_id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String group_name = resultSet.getString(2);
            Groups groups = new Groups(id, group_name);
            groupsList.add(groups);
        }

        resultSet.close();
        preparedStatement.close();
        disconnect();

        return groupsList;
    }
}
