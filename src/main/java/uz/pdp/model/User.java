package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private int id;
    private String first_name;
    private String last_name;
    private String phone_number;
    private String password;

    private String role_name;
    private boolean active;

    public User(int id, boolean active) {
        this.id = id;
        this.active = active;
    }

    public User(String first_name, String last_name, String phone_number, String password) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone_number = phone_number;
        this.password = password;
    }


    public User(int id, String first_name) {
        this.id = id;
        this.first_name = first_name;
    }

    public User(int id, String first_name, boolean active) {
        this.id = id;
        this.first_name = first_name;
        this.active = active;
    }

    public User(String first_name) {
        this.first_name = first_name;
    }
}
