package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Groups {
    private int id;
    private String name;
    private int user_id;

    public Groups(String name, int user_id) {
        this.name = name;
        this.user_id = user_id;
    }

    public Groups(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
