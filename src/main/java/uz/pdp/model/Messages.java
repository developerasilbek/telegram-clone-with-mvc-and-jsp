package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Messages {
    private int id;
    private int fromId;
    private int toId;
    private String text;

    public Messages(int fromId, int toId, String text) {
        this.fromId = fromId;
        this.toId = toId;
        this.text = text;
    }
}
