package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupMembers {
    private int id;
    private int group_id;
    private int member_id;

    public GroupMembers(int group_id, int member_id) {
        this.group_id = group_id;
        this.member_id = member_id;
    }
}
