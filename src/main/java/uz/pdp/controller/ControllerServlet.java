package uz.pdp.controller;

import lombok.SneakyThrows;
import uz.pdp.model.GroupMembers;
import uz.pdp.model.Groups;
import uz.pdp.model.Messages;
import uz.pdp.model.User;
import uz.pdp.service.DataBase;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

public class ControllerServlet extends HttpServlet {
    private DataBase dataBase;

    public void init() {
        String jdbcURL = getServletContext().getInitParameter("jdbcURL");
        String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
        String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");

        dataBase = new DataBase(jdbcURL, jdbcUsername, jdbcPassword);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);

    }

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        PrintWriter printWriter = response.getWriter();

        switch (action) {
            case "/newGroup":
                response.sendRedirect("/insertGroup.jsp");
                break;
            case "/listUsers":
                showAllUsers(request, response);
                break;
            case "/toId":
                HttpSession session = request.getSession();
                Integer user_id = (Integer) session.getAttribute("user_id");
                Integer uId = Integer.valueOf(request.getParameter("uId"));
                boolean active = dataBase.activeUserById(uId);
                boolean ownerActive = dataBase.activeUserById(user_id);
                if (ownerActive) {
                    if (active) {
                        showMessages(request, response);
                    } else {
                        printWriter.write("<h1>this user is blocked</h1>");
                    }
                } else {
                    printWriter.write("<h1>You are blocked</h1>");
                }
                break;
            case "/fromId":
                insertMessages(request, response);
                break;
            case "/listActiveUsers":
                showUsersAdminCabinet(request, response);
                break;
            case "/block":
                updateBlockAndUnBlock(request, response);
                break;
            case "/insertGroup":
                insertGroup(request, response);
                break;
            case "/myGroup":
                showMyGroups(request, response);
                break;
            case "/addMember":
                showUsersInsertForGroup(request, response);
                break;
            case "/addGroupUser":
                insertMemberToGroup(request, response);
                break;
            case "/groupUsers":
                showGroupMembers(request, response);
                break;
            case "/deleteMember":
                deleteUserFromGroup(request, response);
                break;
            case "/listJoinMyGroup":
                showAddedGroup(request, response);
                break;
            case "/leftGroup":
                leftFromJoinGroup(request, response);
                break;
            case "/myInfo":
                getAgent(request, response);
                break;
            case "/logout":
                response.sendRedirect("/login.jsp");
                break;
            case "/exitFromInsertUser":
                response.sendRedirect("/listUsers");
                break;
            case "/exitFromMyGroup":
                response.sendRedirect("/listUsers");
                break;
            case "/exitFromAddUsers":
                response.sendRedirect("/myGroup");
                break;
            case "/exitFromGroupMembers":
                response.sendRedirect("/myGroup");
                break;
            case "/logoutFromAdminCabinet":
                response.sendRedirect("/login.jsp");
                break;
            case "/exitFromAddedGroups":
                response.sendRedirect("/listUsers");
                break;
            case "/logoutFromMessage":
                response.sendRedirect("/listUsers");
                break;
            default:
                break;
        }

    }

    private void showAllUsers(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        List<User> listUser = dataBase.listAllUsers(user_id);
        request.setAttribute("listUser", listUser);
        RequestDispatcher dispatcher = request.getRequestDispatcher("userListForUser.jsp");
        dispatcher.forward(request, response);

    }

    private void showUsersAdminCabinet(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        List<User> listUser = dataBase.listAllUsers(user_id);
        request.setAttribute("listUser", listUser);
        RequestDispatcher dispatcher = request.getRequestDispatcher("adminCabinet.jsp");
        dispatcher.forward(request, response);

    }

    private void showMessages(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        Integer id = Integer.valueOf(request.getParameter("uId"));
        session.setAttribute("toId", id);
        List<Messages> listMessage = dataBase.listOfMessagesByUserId(user_id, id);
        request.setAttribute("listMessage", listMessage);
        List<Messages> listMyFriendMessage = dataBase.listOfMessagesByUserId(id, user_id);
        request.setAttribute("listMyFriendMessage", listMyFriendMessage);
        RequestDispatcher dispatcher = request.getRequestDispatcher("messages.jsp");
        dispatcher.forward(request, response);
    }

    private void insertMessages(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        Integer id = (Integer) session.getAttribute("toId");
        String text = request.getParameter("text");
        Messages messages = new Messages(user_id, id, text);
        dataBase.insertMessage(messages.getFromId(), messages.getToId(), messages.getText());
        response.sendRedirect("/toId?uId=" + id);
    }

    private void updateBlockAndUnBlock(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        boolean active = dataBase.activeUserById(id);
        if (active) {
            User user = new User(id, false);
            dataBase.updateUserBlockAndUnblock(user);
        } else {
            User user = new User(id, true);
            dataBase.updateUserBlockAndUnblock(user);
        }
        response.sendRedirect("/listActiveUsers");

    }

    private void insertGroup(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String name = request.getParameter("name");
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        dataBase.insertGroup(name, user_id);
        response.sendRedirect("userListForUser.jsp");
    }

    private void showMyGroups(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        List<Groups> groupsList = dataBase.listMyGroups(user_id);
        request.setAttribute("listGroup", groupsList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("myGroups.jsp");
        dispatcher.forward(request, response);
    }

    private void showUsersInsertForGroup(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        Integer group_id = Integer.valueOf(request.getParameter("groupId"));
        session.setAttribute("group_id", group_id);
        List<User> userList = dataBase.listInsertUserForGroup(user_id, group_id);
        request.setAttribute("ListGroupUser", userList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("addUsers.jsp");
        dispatcher.forward(request, response);
    }

    private void insertMemberToGroup(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        HttpSession session = request.getSession();
        Integer group_id = (Integer) session.getAttribute("group_id");
        Integer id = Integer.valueOf(request.getParameter("id"));
        dataBase.insertMemberToGroup(group_id, id);
        response.sendRedirect("/addMember?groupId=" + group_id);
    }

    private void showGroupMembers(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        Integer group_id = Integer.valueOf(request.getParameter("id"));
        session.setAttribute("group_id", group_id);
        List<User> userList = dataBase.listOfGroupMembers(user_id, group_id);
        request.setAttribute("listUser", userList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("groupMembers.jsp");
        dispatcher.forward(request, response);
    }

    private void deleteUserFromGroup(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        HttpSession session = request.getSession();
        Integer group_id = (Integer) session.getAttribute("group_id");
        Integer userId = Integer.valueOf(request.getParameter("userId"));
        GroupMembers groupMembers = new GroupMembers(group_id, userId);
        dataBase.deleteMembersFromGroup(group_id, userId);
        response.sendRedirect("groupUsers?id=" + group_id);
    }

    private void leftFromJoinGroup(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        Integer gId = Integer.valueOf(request.getParameter("gId"));
        GroupMembers groupMembers = new GroupMembers(gId, user_id);
        dataBase.deleteMembersFromGroup(gId, user_id);
        response.sendRedirect("/listJoinMyGroup");
    }

    private void showAddedGroup(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        HttpSession session = request.getSession();
        Integer user_id = (Integer) session.getAttribute("user_id");
        List<Groups> groupsList = dataBase.listAddedGroup(user_id);
        request.setAttribute("listAddedGroup", groupsList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("addedGroups.jsp");
        dispatcher.forward(request, response);
    }

    public void getAgent(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter printWriter = response.getWriter();
        String header = request.getHeader("USER-AGENT");
        printWriter.write(header);
    }
}
