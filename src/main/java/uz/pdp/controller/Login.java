package uz.pdp.controller;

import uz.pdp.model.User;
import uz.pdp.service.DBService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(value = "/login")
public class Login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("login.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String phoneNumber = req.getParameter("phoneNumber");
        String password = req.getParameter("password");
        DBService dbService = new DBService();
        User user = dbService.login(phoneNumber, password);
        PrintWriter printWriter = resp.getWriter();
        if (user == null) {
            printWriter.write("<h1>Password or login error</h1>");
        } else {
            HttpSession session = req.getSession();
            session.setAttribute("user_id", user.getId());
            if (user.getRole_name().equals("admin")) {
                resp.sendRedirect("/adminCabinet.jsp");
            } else {
                resp.sendRedirect("/userListForUser.jsp");
            }
        }
    }
}
