package uz.pdp.controller;

import uz.pdp.model.Result;
import uz.pdp.model.User;
import uz.pdp.service.DBService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Register extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/register.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String phoneNumber = req.getParameter("phoneNumber");
        String password = req.getParameter("password");
        String prePassword = req.getParameter("prePassword");
        PrintWriter printWriter = resp.getWriter();
        if (password.equals(prePassword)) {
            DBService dbService = new DBService();
            User user = new User(firstName, lastName, phoneNumber, password);
            Result result = dbService.registerUser(user);
            if (result.isSuccess()) {
                resp.sendRedirect("/login.jsp");
            } else {
                resp.sendRedirect("/login.jsp");
            }
        } else {
            printWriter.write("<h1 color='red'>Password not equals</h1>");
        }
    }
}
