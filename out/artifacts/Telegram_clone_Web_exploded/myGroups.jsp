<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/17/2022
  Time: 10:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div align="center">
  <table border="1" cellpadding="5">
    <caption><h2>List of Groups</h2></caption>
    <tr>
      <th>Name</th>
      <th>Action</th>
    </tr>
    <c:forEach var="groups" items="${listGroup}">
      <tr>

        <td><c:out value="${groups.name}" /></td>
        <td>
          <a href="/addMember?groupId=<c:out value='${groups.id}'/>">Add Member</a>
          &nbsp;&nbsp;&nbsp;
          <a href="/groupUsers?id=<c:out value='${groups.id}' />">Group users</a>
          &nbsp;&nbsp;&nbsp;

        </td>
      </tr>
    </c:forEach>
  </table>
  <h2>
    <a href="/exitFromMyGroup">Exit</a>
  </h2>
</div>

</body>
</html>
