<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/16/2022
  Time: 6:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All Users</title>
</head>
<body>
<center>
    <h1>Telegram</h1>
    <h2>
        <a href="/myGroup">My Groups</a>
        &nbsp;&nbsp;
        <a href="/newGroup">Create Group</a>
        &nbsp;&nbsp;&nbsp;
        <a href="/listUsers">My Contact</a>
        &nbsp;&nbsp;&nbsp;
        <a href="/listJoinMyGroup">Groups</a>
        &nbsp;&nbsp;&nbsp;
        <a href="/myInfo">MyInfo</a>

    </h2>
</center>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>List of Users</h2></caption>
        <tr>
            <th>Name</th>
            <th>Action</th>
        </tr>
        <c:forEach var="user" items="${listUser}">
            <tr>

                <td><c:out value="${user.first_name}" /></td>
                <td>
                    <a href="/toId?uId=<c:out value='${user.id}' />">SendMessage</a>

                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<center>

    <h2>
        <a href="/logout">Logout</a>
    </h2>
</center>
</body>
</html>
