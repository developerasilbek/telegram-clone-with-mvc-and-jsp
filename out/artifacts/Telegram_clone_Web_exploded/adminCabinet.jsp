<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/17/2022
  Time: 8:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Cabinet</title>
</head>
<body>

<center>
    <h1>Telegram</h1>
    <h2>
<%--        <a href="/newGroup">Add Group</a>--%>
        &nbsp;&nbsp;&nbsp;
        <a href="/listActiveUsers">Users</a>

    </h2>
</center>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>List of Users</h2></caption>
        <tr>
            <th>Name</th>
            <th>Action</th>
        </tr>
        <c:forEach var="user" items="${listUser}">
            <tr>
                <td bgcolor="${user.active?"green":"red"}"><c:out value="${user.first_name}" /></td>
                <td>
                    <a href="/block?id=<c:out value='${user.id}' />">${user.active?"Block":"Unblock"}</a>

                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<center>

    <h2>
        <a href="/logoutFromAdminCabinet">Logout</a>
    </h2>
</center>
</body>
</html>
